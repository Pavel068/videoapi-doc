# Version 5.7


## Сервер (production):
- V.1.0 http://52.34.139.66/API/{api_method}/
- V.2.0 http://52.34.139.66/API_V_2/{api_method}/ - текущий

## Header
#### Передается по всех запросах, кроме /getaccess, /auth, /register
#### Поля:
- id: int,
- token: string // токен, полученный от запроса токена

#### Response в случае ошибки (нет доступа к API):
```
{
	"response":
	{
		"error": "unauthorized"
	}
}
```
#### Response в случае успеха - соответствует респонсу запрашиваемого метода

## 1. Запрос токена
#### Метод: POST
#### url: /getaccess/

#### Параметры:
- userId: int,
- fbToken: string // токен фейсбука

#### Response:
```
{
	"response":
	{
		"status": bool,
		"token": token // если status=1, этот токен будет использоваться при дальнейших запросах,
		"userId": int
	}
}
```

## 2. Загрузка видео
#### Метод: POST
#### url: /upload/

#### Параметры:
- source: строка base64 вида: "data:image/png;base64,iVBO...",
- videoname: string,
- preview: строка base64 вида: "data:image/png;base64,iVBO...",
- name: string,
- avatar: строка base64 вида: "data:image/png;base64,iVBO...",
- geo: string,
- fileSource: string,
- lat: double, // широта
- lng: double, // долгота
- is_public: bool

#### Response:
```
{  
	"response":  
	{  
		id: int,  
		filename: string, // это имя файла на сервере (генерируется случайно)  
		videoname: string, // это произвольное название видео,  
		preview: string, // картинка-превью  
		uploadDate: timestamp,
		username: string,
		fileSource: string,
		description: string,
		name: string,
	}  
}
```

## Загрузка медиа (новая версия)
#### Метод: POST
#### url: /newupload/

#### Параметры:
- tags: [string],
- videoname: string,
- isSourceLink: bool,
- source: file or string, // зависит от isSourceLink
- isPreviewLink: bool,
- preview: file or string, // зависит от isPreviewLink
- geo: string,
- fileSource: string, // а по факту - номер фичи (1 - эмоджи, 15 - рисунок и т.д.)
- lat: double, // широта
- lng: double, // долгота
- is_public: bool
- mediaType: int // если не передавать это поле, то будет записано 0
- imgWidth: int
- imgHeight: int
- videoLink: string

## Загрузка файла по url
#### Метод: POST
#### url: /uploadbylink/

#### Параметры - такие же, как при обычной загрузке (/newupload), за исключением source:
- source: string (линк на файл)

## 3. Получение списка видео
#### Метод: GET
#### url: /list/{user_id}/

#### Response:
```
{
	"response":
	[
		{
			id: int,
			videoname: string, // это имя файла на сервере (генерируется случайно)
			filename: string, // это произвольное название видео
			preview: string,
			uploadDate: timestamp,
			name: string,
			is_public: bool,
			user_id: int,
			avatar: string,
			geo: string,
			lat: double,
			lng: double,
			imgWidth: int,
			imgHeight: int			
		}		
	]
}
```

## 4. Изменение приватности видео
#### Метод: POST
#### url: /changepriv/

#### Параметры:
- videoId: int,
- is_public: bool

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 5. Фид
#### Метод: POST
#### url: /feed/`

#### Параметры:
- timezone: float,
- timeCorrection: int (UNIX timestamp), // время загрузки фида (offset=0), при пагинации передается БЕЗ изменения
- type: bool, // 1 - с группировкой по дням и юзерам, 0 - группировка по юзерам
- offset: int,
- limit: int
- filter: int // 0 - нет фильтра, 1 - featured, 2 - nearby, 3 - popular, 4 - pictures, 5 - не показывать свои коллекции + фильтр по тегам и name
- filterParam: string


#### Response:
```
{
	"response":
		"totalFileItems": int,
		"filter": int,
		"feed":
		[
			{
				fbId: string,
				avatar: string,
				name: string,
				lastUpload: datetime,
				isSelfGifExists: bool,
				isFollowed: bool,
				geo: string,
				files:
				[
					{
						id: int,
						fromFB: bool,
						videoname: string,
						preview: string,
						filename: string,
						fileSource: string,
						featured: bool,
						likes: int,
						isSourceLink: bool,
						isPreviewLink: bool,
						shares: int,
						imgWidth: int,
						imgHeight: int,						
						myLike: bool,
						myShare: bool,				
						uploadDate: datetime,
						tags: [string]
					}
				],
				shares: int,
				featured: bool,
				totalLikes: int
			}
		]
}
```

## 6. Follow/Unfollow
#### Метод: POST
#### url: /follow/

#### Параметры:
- followId: int // id, который фолловим
- follow: bool // 1 - follow, 0 - unfollow

#### Response:
```
{
	"response":
	{
		"follow": bool,
		"user_id": int,
		"username": string,
		"avatar": string,
		"geo": string,
		...
	}
}
```

## 7. Поиск по имени
#### Метод: POST
#### url: /search/

#### Параметры:
- name: string
- timezone: float

#### Response:
```
{
	"response":
	[
		{
			request: string,
			name: string,
			userId: int,
			avatar: string,
			isSelfGifExists: bool,
			geo: string,
			follow: bool,
			follows:
			[
				{
					name: string,
					user_id: int,
					avatar: string,
					geo: string,
					description: string
				}
			],
			lastCollection:
			[
				{
					id: int,
					videoname: string,
					filename: string,
					preview: string,
					uploadDate: timestamp
				}
			]
		}
	]
}
```

## 8. Follow feed без группировок
#### Метод: POST
#### url: /followfeed/
#### Параметры и респонс - аналогично обычному фиду

## 9. Получение списка пользователей
#### Метод: GET
#### url: /userslist/

#### Response:
```
{
	"response":
	[
		{
			user_id: int,
			name: string,
			avatar: string,
			geo: string
		}
	]
}
```

## 10. Получение профиля пользователя
#### Метод: POST
#### url: /user/

#### Параметры:
- userId: int,
- timezone: float
- group: int // 1 - с группировкой файлов, 0 - без группировки. Если не передавать - 1, по умолчанию

#### Response:
```
{
	"response":
	{
		info:
		{
			user_id: int
			name: string
			avatar: string
			geo: string
			description: string
			isSelfGifExists: bool
			emojiAvailable: bool
		},
		totalFiles: int,
		follow: [
			user_id: int
			name: string
			avatar: string
			geo: string
			description: string
			follow: bool
		]
		followedBy: [
			user_id: int
			name: string
			avatar: string
			geo: string
			description: string
			follow: bool
		]
		totalShares: int,
		totalLikes: int,
		mostFrequentEmoji: string
		files: [
			{
				id: int
				videoname: string
				filename: string
				preview: string
				uploadDate: datetime
				fileSource: string
				imgWidth: int
				imgHeight: int				
				is_public: bool
				isSourceLink: bool
				isPreviewLink: bool
				likes: int,
				tags:[string]
			}
		]
	}
}
```

## 11. Добавление/обновление description в профиле
#### Метод: POST
#### url: /description/

#### Параметры:
- description: string (до 200 символов)

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 12. Шеринг файла из коллекции
#### Метод: POST
#### url: /share/

#### Параметры:
- fileId: int // файл, который шерим

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 13. Поставить/убрать лайк к файлу
#### Метод: POST
#### url: /like/

#### Параметры:
- fileId: int,
- like: bool

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 14. Зашерить коллекцию
#### Метод: POST
#### url: /sharecollection/

#### Параметры:
- files: [int] // id файлов коллекции, которую шерим

#### Response:
```
{
	"response":
	{
		"status": bool,
		"collId": int
	}
}
```

## 15. Получение зешеренной коллекции
#### Метод: POST
#### url: /getsharedcollection/

#### Параметры:
- collId: int

#### Response:
```
{
	"response":
	[
		{
			id: int,
			filename: string,
			preview: string,
			videoname: string,
			user_id: int // id загрузившего
			uploadDate: datetime // не учитывается часовой пояс (!)
		}
	]
}
```

## 16. Создание/обновление username
#### Метод: POST
#### url: /username/

#### Параметры:
- username: string // начинается с @ (как в telegram)

#### Response:
```
{
	"response":
	{
		"status": bool,
		"error": bool
	}
}
```

## 17. Поиск по username
#### Метод: POST
#### url: /getusername/

#### Параметры:
- username: string // с "@" начинается
- timezone: double

#### Response: (такой же, как у /user)
```
{
	"response":
	{
		info:
		{
			user_id: int
			name: string
			avatar: string
			geo: string
			description: string
			isSelfGifExists: bool
		},
		totalFiles: int,
		follow: [
			user_id: int
			name: string
			avatar: string
			geo: string
			description: string
		]
		followedBy: [
			user_id: int
			name: string
			avatar: string
			geo: string
			description: string
		]
		totalShares: int,
		totalLikes: int,
		files: [
			{
				id: int
				videoname: string
				filename: string
				preview: string
				uploadDate: datetime
				is_public: bool
				likes: int
			}
		]
	}
}
```

## 18. Поиск друзей fb в базе данных
#### Метод: POST
#### url: /findfriends/

#### Параметры:
- friends: [int] // массив id друзей в фейсбуке

#### Response:
```
{
	"response":
	[
		{
			"userId": int
			"name": string
			"avatar": string
			"geo": string
			"description": string
			"lat": double
			"lng": double
			"username": string
		}
	]
}
```

## 20. Удаление файла
#### Метод: POST
#### url: /deletefile/

#### Параметры:
- fileId: int

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 21. Пожаловаться на файл
#### Метод: POST
#### url: /abusefile/

#### Параметры:
- fileId: int
- reason: string

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 22. Фид для сайта
#### Метод: POST
#### url: /sitefeed/

#### Параметры:
- timezone: float,
- timeCorrection: int (UNIX timestamp), // время загрузки фида (offset=0), при пагинации передается БЕЗ изменения
- type: bool, // 1 - с группировкой по дням и юзерам, 0 - группировка по юзерам
- offset: int,
- limit: int
- filter: int // 0 - нет фильтра, 1 - featured, 2 - nearby, 3 - popular
- lat: float // широта пользователя
- lng: float // долгота пользователя


#### Response:
```
совпадает с response обычного фида для приложения
```

## 23. Добавление комментария к юзеру
#### Метод: POST
#### url: /addcomment/

#### Параметры:
- userId: int,
- commentText: string

#### Response:
```
{
	"response":
	{
		"status": bool,
		"commentId": int,
		"commentText": string
	}
}
```

## 23.1. Получение комментариев
#### Метод: POST
#### url: /getcomments/

#### Параметры:
- userId: int,
- offset: int,
- limit: int
- type: int // 1 - комментарии К userId, 2 - комментарии ОТ userId
- timezone: double

#### Response:
```
{
  "response": {
	"feed": [
	  {
	    "userId": int,
	    "fileId": int,
	    "commentId": int,
	    "commentText": string,
	    "eventTime": datetime,
	    "name": string,
	    "username": string,
	    "avatar": string
	  },
	  "totalComments": int
}
```

## 23.2 Удаление комментария
#### Метод: POST
#### url: /deletecomment/

#### Параметры:
- commentId: int

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 24. Запись тегов для существующего файла
#### Метод: POST
#### url: /addtags/

# Параметры:
- tags: [string],
- fileId: int

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 25. Поиск файлов по тегам
#### Метод: POST
#### url: /searchtag/

#### Параметры:
- tag: string,
- offset: int,
- limit: int,
- timeCorrection: timestamp,
- timezone: double


#### Response:
```
совпадает с response обычного фида для приложения
```

## 26. Отправка deviceToken для юзера
#### Метод: POST
#### url: /setdevice/

#### Параметры:
- deviceToken: string

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 27. Список лайков и шеров для пользователя
#### Метод: POST
#### url: /usersharesnlikes/

#### Параметры:
- userId: int
- offset: int,
- limit: int,
- timezone: int,
- type: 0 - лайки, 1 - шеры, 2 - все в кучу
- reverse: bool

#### Response:
```
{
	"response":
		"type": int // запрошенный тип выдачи
		"totalFileItems": int,
		"totalLikes": int,
		"totalShares": int,
		"feed":
		[
			{
				"eventType": "like" or "share" or "browsing"
				"preview": string
				"filename" :string
				"id": int // айди файла
				"mediaType": int
				"fileSource": string
				"isPreviewLink": int
				"isSourceLink": int
				"eventTime": timestamp
				"userId": int
				"name": string
				"avatar": string		
			}
		]
}
```

## 28. Фид от друзей фейсбука
#### Метод: POST
#### url: /fbfriendfeed/

#### Параметры:
- timezone: float,
- timeCorrection: int (UNIX timestamp), // время загрузки фида (offset=0), при пагинации передается БЕЗ изменения
- type: bool, // 1 - с группировкой по дням и юзерам, 0 - группировка по юзерам
- offset: int,
- limit: int
- filter: int // 0 - нет фильтра, 1 - featured, 2 - nearby, 3 - popular

#### Response:
```
аналогично обычному фиду
```

## 29. Отправка на сервер линка аватара (для работы с чатботом)
#### Метод: POST
#### url: /addavalink/

#### Параметры:
- avatarLink: string

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 30. Получение списка друзей фейсбука
#### Метод: POST
#### url: /getfbfriendlist/

#### Параметры:
- avatarLink: string

#### Response:
```
{
	"haveFriends": bool,
	"response":
	[
		{
			userId: int,
			name: string,
			avatar: string,
			username: string,
			lastEmoji: string,
			isSelfGifExists: bool,
			files: 
			[
				"id": int
				"filename": string
				"preview": string
				"uploadDate": timestamp
				"isPreviewLink": int
				"isSourceLink": int
				"fileSource": string
				"mediaType": int				
			]
		}
	]
}
```

## 31. Запрос получения ID юзера по avatarLink
#### Метод: POST
#### url: /getuserid/

#### Параметры:
- avatarLink: string

#### Response:
```
{
	"response":
	{
		"exists": bool,
		"userId": int
	}
}
```

## 32. Выполнение команды ботом
#### Метод: POST
#### url: /execcommand/

#### Параметры:
- phrase: string

#### Response:
```
Как у /getfbfriendlist
```

## 33. Запрос смены аватара
#### Метод: POST
#### url: /changeavatar/

#### Параметры:
- isAvatarLink: bool,
- avatar: file or string

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 34. Общий запрос на изменение профиля
#### Метод: POST
#### url: /update/

#### Параметры:
- isAvatarLink: bool, // обязателен, если передается аватар
- avatar: file or string
- name: string,
- username: string,
- description: string

#### Response:
```
{
	"response":
	{
		"status": bool,
		"error": int // 0 - нет ошибки, 1 - существующий username, 2 - нет медиа (при загрузке аватара как ссылки), 3 - иная ошибка
	}
}
```

## 35. Регистрация пользователя через email
#### Метод: POST
#### url: /register/

#### Параметры:
- email: string
- password: string
- name: string,
- avatar: file
- username: string

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 36. Авторизация пользователя через email
#### Метод: POST
#### url: /auth/

#### Параметры:
- email: string
- password: string

#### Response:
```
{
	"response":
	{
		userId: int,
		token: string,
		name: string,
		avatar: string,
		username: string
		// в случае ошибки
		error:1
	}
}
```

## 37. Восстановление пароля
#### Метод: POST
#### url: /retrievepass/

#### Параметры:
- email: string

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
// в случае status = 1 на почту приходит линк для смены пароля. Status = 0 - нет такого email в БД
```

## 38. Запрос просмотра
#### Метод: POST
#### url: /browsing/

#### Параметры:
- userId: int
- browsingId: int

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 39. Запрос получения списка категорий
#### Метод: GET
#### url: /catlist/

#### Response:
```
{
	"response":
	{
		"categories": [string]
	}
}
```

## 40. Запрос получения данных пользователя по временному токену
#### Метод: POST
#### url: /getcredentials/

#### Параметры:
- tmpToken: string

#### Response:
```
{
	"response":
	{
		userId: int,
		name: string,
		username: string,
		avatar: string,
		token: string
	}
}
```

## 41. HOT фид
#### Метод: POST
#### url: /hotfeed/

#### Параметры:
- isShort: bool

#### Response:
````
{
	"response":
	[
		{
			id: int,
			videoname: string,
			preview: string,
			filename: string,
			fileSource: string,
			isSourceLink: bool,
			isPreviewLink: bool,
			imgWidth: int,
			imgHeight: int,										
			uploadDate: datetime,
            shares: int,
            likes: int,
            myShare: int,
            myLike: int			
		}
	]
}
````

## 42. Загрузка видео и генерация гифки на стороне сервера
#### Метод: POST
#### url: /uploadvideo/

#### Параметры:
- tags: [string],
- source: file
- imgWidth: int,
- imgHeight: int,
- fileSource: int, // номер фичи (1 - эмоджи, 15 - рисунок и т.д.)
- mediaType: int // если не передавать это поле, то будет записано 0

#### Response:
```
{  
	"response":  
	{  
        "name": "string",
        "avatar": "string",
        "description": "string",
        "username": "string",
        "id": int,
        "videoname": "string",
        "preview": "string",
        "filename": "string",
        "uploadDate": "datetime",
        "userId": int,
        "is_public": int,
        "fileSource": int,
        "isLink": int,
        "mediaType": int,
        "imgWidth": int,
        "imgHeight": int,
        "isSourceLink": int,
        "isPreviewLink": int,
        "isFeedDuplicate": int,
        "videoLink": "string",
        "originId": int,
        "giflink": "string"
	}  
}
```

## 43. Запрос создания эмоджи
#### Метод: POST
#### url: /requestemoji/

#### Параметры:
- userId: int // кому шлем запрос
- emoji: string

#### Response:
```
{
	"response":
	{
		"status": bool
	}
}
```

## 44. Список запрошенных эмоджи для юзера
#### Метод: POST
#### url: /getemoji/

#### Параметры:
- userId: int

#### Response:
```
{
	"response":
	[
		{
			"emoji": string,
			"count": int,
			"requests": [
				{
					userId: int,
					name: string,
					avatar: string
				}
			]
		}
	]
}
```



## Коды ошибок при загрузке файла
- 1 - дубликат (такой же файл был загружен не позже 24 часов назад)
- 2 - дубликат файла, зашеренного по линку
- 3 - отсутствие медиа в зашеренном файле
- 4 - неизвестная ошибка
- 5 - битый файл без расширения
- 6 - неопознанный пользователь (в хидере не пришел id юзера)